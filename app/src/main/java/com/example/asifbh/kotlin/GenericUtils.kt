package com.example.asifbh.kotlin

import android.app.Activity
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*


class GenericUtils() {

    val TAG: String="GenericUtils.class"
    var imageDAO = ImageDAO()

    /*
    * method gether all info of photo and pass it to DAO Class
    * returns void
    * */
    fun addImagesToLocalStorage(activity: Activity){
        Log.i(TAG,"Inside addImagesToLocalStorage() method")
        val uri: Uri
        val cursor: Cursor?
        val column_index_data: Int
        val column_created_date: Int
        val uniqueID:Int
        val listOfAllImages = ArrayList<ImageDTO>()
        uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        Log.i(TAG,uri.toString())

        val projection = arrayOf(
            MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.MediaColumns.DATE_ADDED,
            MediaStore.MediaColumns.DATE_MODIFIED, MediaStore.MediaColumns._ID)

        cursor = activity.contentResolver.query(uri, projection, null, null, null)

        column_index_data = cursor!!.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
        column_created_date = cursor!!.getColumnIndexOrThrow(MediaStore.MediaColumns.DATE_ADDED)
        uniqueID= cursor!!.getColumnIndexOrThrow(MediaStore.MediaColumns._ID)

        while (cursor!!.moveToNext()) {
            var absolutePathOfImage = cursor?.getString(column_index_data)
            Log.i(TAG,absolutePathOfImage.toString())
            var dateCreated=cursor?.getString(column_created_date)
            Log.i(TAG,dateCreated.toString())
            var imageID= cursor?.getString(uniqueID)
            Log.i(TAG,imageID.toString())
            var byteArray = getThumbnail(absolutePathOfImage)
            Log.i(TAG,"Populating ImageDTO for saving")
            var imageDto = ImageDTO()
            imageDto.id=imageID.toInt()
            imageDto.srcPath=absolutePathOfImage
            imageDto.createdTime=dateCreated
            imageDto.thumbnail=byteArray
            listOfAllImages.add(imageDto)
        }
        imageDAO.addImage(listOfAllImages)
    }

    /*
    * method generate byte array of image thumbnail
    * returns byte Array of thumbnail
    * */
    private fun getThumbnail(imagePath: String): ByteArray {
        Log.i(TAG,"Inside getThumbnail() method")
        Log.i(TAG,"Fetching thumbnail for image at path $imagePath")
        val thumbImage = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(imagePath),128,128)
        val stream = ByteArrayOutputStream()
        thumbImage.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val byteArray = stream.toByteArray()
        thumbImage.recycle()
        Log.i(TAG,"Thumbnail fetch sucessfully")
        return byteArray
    }

    /*
    * method returns list of images which is deleted from file storage
    * returns list of deleted images
    * */
    public fun getListOFDeletedImages(listOfAllImages: MutableList<ImageDTO>): MutableList<ImageDTO>{
        Log.i(TAG,"Inside getListOfDeletedImages() method")
        var listOfDeletedImages: MutableList<ImageDTO> = mutableListOf<ImageDTO>()
        for (i in 0 until listOfAllImages.size) {
            val image = listOfAllImages[i]
            var sourceLocation = image.srcPath
            if (!checkImageExist(sourceLocation)) {
                Log.i(TAG,"No image found our at source Location $sourceLocation Adding it into list")
                listOfDeletedImages.add(image)
            }
        }
        return  listOfDeletedImages
    }

    /*
    * method checks if images exist in fileSystem or not
    * return boolean flag
    * */
    public fun checkImageExist(filePath: String): Boolean{
        Log.i(TAG,"Inside checkImageExist() method")
        var fileExistFlag = true
        var file = File(filePath)
        if(!file.exists()){
            fileExistFlag=false
        }
        return fileExistFlag
    }
}