package com.example.asifbh.kotlin

import android.content.Context
import android.content.Intent
import android.database.DataSetObserver
import android.graphics.BitmapFactory
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ListAdapter
import android.widget.RelativeLayout
import com.example.asifbh.kotlin.ImageDTO
import com.example.asifbh.kotlin.R
import kotlinx.android.synthetic.main.list_view_layout.view.*


/*
* Data bidding Adapter of Recycle View
* binds Data to layout
* */
class ImageAdapter(private val mContext: Context, private val imagelist: MutableList<ImageDTO>) : RecyclerView.Adapter<ImageAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_view_layout, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val imageDTO = imagelist.get(position)
        val data = imageDTO.thumbnail
        val bmp = BitmapFactory.decodeByteArray(data, 0, data.size)
        holder.mainLayout.imageThubnail.setImageBitmap(bmp)
    }

    override fun getItemCount(): Int {
        return imagelist.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

         var mainLayout: LinearLayout

        init {
            mainLayout = itemView.listView
        }
    }
}
