package com.example.asifbh.kotlin

import android.nfc.Tag
import android.util.Log
import io.realm.Realm
import io.realm.RealmResults
import java.lang.Exception

import java.util.LinkedList
import java.util.logging.Logger

class ImageDAO internal constructor() {
    val TAG: String="ImageDAO.class"

    internal var realm: Realm

    init {
        this.realm = Realm.getDefaultInstance()
    }


    /*
    * method for get All images from local storage
    * returns listofimages
    * */

    fun getAllImage(): MutableList<ImageDTO> {
        var listOfThumbnail: MutableList<ImageDTO> = mutableListOf<ImageDTO>()
        try {
            Log.i(TAG,"Inside getAllImage() method")
            val resultSet = realm.where(ImageDTO::class.java).findAll()
            if(resultSet.isNotEmpty()){
                listOfThumbnail = realm.copyFromRealm(resultSet)
            }
        }catch (e: Exception){
            Log.e(TAG,"Exception Occured while fetching Image from DB")
            Log.e(TAG,e.message)
        }
        Log.i(TAG,"Returning list of image thumbnail")
        return listOfThumbnail
        }

    /*
    * method for add  images to local storage
    * returns void
    * */
    fun addImage(listOfAllImages: List<ImageDTO>) {
        Log.i(TAG,"Inside addImage() Method")
        try {
            realm.beginTransaction()
            Log.i(TAG,"Inserting List of Images to Local Storage")
            realm.insertOrUpdate(listOfAllImages)
            realm.commitTransaction()
        }catch (e: Exception){
            Log.e(TAG,"Exception Occured while adding list of images to Local storage")
            Log.e(TAG,"Exception Occured "+e.message)
        }

    }
}