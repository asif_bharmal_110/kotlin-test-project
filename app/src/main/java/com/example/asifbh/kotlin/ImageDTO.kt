package com.example.asifbh.kotlin


import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass



@RealmClass
open class ImageDTO(): RealmObject() {
    @PrimaryKey
    var id:Int=0
    var srcPath:String=""
    var thumbnail:ByteArray = byteArrayOf()
    var createdTime: String=""
}