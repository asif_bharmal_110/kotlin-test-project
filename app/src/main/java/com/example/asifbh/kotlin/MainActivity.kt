package com.example.asifbh.kotlin

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem

import kotlinx.android.synthetic.main.activity_main.*
import java.util.logging.Logger
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.support.design.widget.TabLayout
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.content_main.*
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {

    private lateinit var recycleView: RecyclerView

    val TAG: String="MainActivity.class"

    var imageDao = ImageDAO()

    var genericUtils = GenericUtils()

    var listOfDeletedImages: MutableList<ImageDTO> = mutableListOf<ImageDTO>()

    var listOfAllImages: MutableList<ImageDTO> = mutableListOf<ImageDTO>()

    /*
    * OnCreate method of Android life cycle
    * is called when app is launched for first time
    * set all layouts and click listeners
    * */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i(TAG,"Inside onCreate() method")
        setContentView(R.layout.activity_main);
        recycleView = findViewById<RecyclerView>(R.id.recycleView)
        recycleView.layoutManager = LinearLayoutManager(this)
        recycleView.addItemDecoration(DividerItemDecoration(this,DividerItemDecoration.VERTICAL))
        var getAllImagesButton = findViewById<Button>(R.id.allImages)
        var getDeletedImages = findViewById<Button>(R.id.deletedImages)

        getAllImagesButton.setOnClickListener {
            if(listOfAllImages.isEmpty()){
                Log.i(TAG,"No Images Found In storage redirecting to other page")
                val intent= Intent(this,NoImageFoundOnStorage::class.java)
                startActivity(intent)
            }else {
                val adapter = ImageAdapter(this, listOfAllImages)
                recycleView.adapter = adapter
            }
        }

        getDeletedImages.setOnClickListener {
            if(listOfDeletedImages.isEmpty()){
                Log.i(TAG,"No Images Deleted redirecting to other page")
                val intent= Intent(this,NoImagesDeleted::class.java)
                startActivity(intent)
            }else{
                val adapter = ImageAdapter(this, listOfDeletedImages)
                recycleView.adapter = adapter
            }

        }

    }

    /*
    * onresume method of Android life cycle
    * is called when app comes to fore front after minimization
    * scan gallery everytime after minimization
    * */
    override fun onResume() {
        super.onResume()
        listOfAllImages.clear()
        listOfDeletedImages.clear()
        genericUtils.addImagesToLocalStorage(this)
        listOfAllImages = imageDao.getAllImage()
        listOfDeletedImages= genericUtils.getListOFDeletedImages(listOfAllImages)
    }


}

