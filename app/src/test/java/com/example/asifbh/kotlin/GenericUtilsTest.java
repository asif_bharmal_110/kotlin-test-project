package com.example.asifbh.kotlin;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.log.RealmLog;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.rule.PowerMockRule;
import org.robolectric.fakes.RoboCursor;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Realm.class,RealmLog.class,Log.class})
public class GenericUtilsTest {


    private  Realm mrealm;

    private GenericUtils genericUtils;


    @Before
    public  void beforeClass(){
        mockStatic(Realm.class);
        mockStatic(RealmLog.class);

        //RealmResults mresults= PowerMockito.mock(RealmResults.class);
        mrealm = mock(Realm.class);
        when(Realm.getDefaultInstance()).thenReturn(mrealm);
        RealmConfiguration mRealmConfiguration = mock(RealmConfiguration.class);

        genericUtils = new GenericUtils();
    }

    @Test
    public void testFileDontExistShouldReturnFalse(){
        try {
            boolean fileExistFlag= genericUtils.checkImageExist("/test/path");
            Assert.assertEquals(false,fileExistFlag);
        }catch (Exception e){
            Assert.fail();
        }
    }

    @Test
    public void testFileExistShouldReturnTrue(){
        try {
            boolean fileExistFlag = genericUtils.checkImageExist("../app/src/test/java/resources/test.jpg");
            Assert.assertEquals(true,fileExistFlag);
        }catch (Exception e){
            Assert.fail();
        }
    }

    @Test
    public void testGetlistOfDeletedImagesShouldReturnValueIfImageAtPathNotPresent(){
        try{
            LinkedList<ImageDTO> listOFAllImages = new LinkedList<ImageDTO>();
            ImageDTO imageDTO = new ImageDTO();
            String thumbnail = "q212312423e4q32e2345234234";
            byte [] thumbnailByte = thumbnail.getBytes();
            imageDTO.setThumbnail(thumbnailByte);
            imageDTO.setCreatedTime("123456");
            imageDTO.setSrcPath("/test/path");
            imageDTO.setId(12);
            listOFAllImages.add(imageDTO);
            List<ImageDTO> listOfDeletedImages =genericUtils.getListOFDeletedImages(listOFAllImages);
            Assert.assertEquals(1,listOfDeletedImages.size());
            Assert.assertEquals(12,listOfDeletedImages.get(0).getId());
        }catch (Exception e){
            Assert.fail();
        }
    }

    @Test
    public void testGetListOFDeletedFilesIfNoImagesDeleted(){
        try{
            LinkedList<ImageDTO> listOFAllImages = new LinkedList<ImageDTO>();
            ImageDTO imageDTO = new ImageDTO();
            String thumbnail = "q212312423e4q32e2345234234";
            byte [] thumbnailByte = thumbnail.getBytes();
            imageDTO.setThumbnail(thumbnailByte);
            imageDTO.setCreatedTime("123456");
            imageDTO.setSrcPath("../app/src/test/java/resources/test.jpg");
            imageDTO.setId(12);
            listOFAllImages.add(imageDTO);
            List<ImageDTO> listOfDeletedImages =genericUtils.getListOFDeletedImages(listOFAllImages);
            Assert.assertEquals(0,listOfDeletedImages.size());
        }catch (Exception e){
            Assert.fail();
        }
    }
}
