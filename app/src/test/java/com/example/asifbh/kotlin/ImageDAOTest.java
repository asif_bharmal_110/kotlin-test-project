package com.example.asifbh.kotlin;

import android.app.Person;
import android.content.Context;
import android.util.Log;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.internal.RealmCore;
import io.realm.log.RealmLog;
import kotlin.jvm.internal.markers.KMutableList;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.rule.PowerMockRule;
import org.robolectric.res.FileTypedResource;

import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import static java.sql.DriverManager.println;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Realm.class,RealmLog.class,Log.class,RealmResults.class})
public class ImageDAOTest {

    @Rule
    public PowerMockRule rule = new PowerMockRule();


    public static Realm mrealm;

    private static RealmResults<ImageDTO> image;

    public static RealmResults<ImageDTO> mrealmResults;


    @InjectMocks
    ImageDAO imageDao = new ImageDAO();

    @BeforeClass
    public static void setUp(){
        mockStatic(RealmLog.class);
        mockStatic(Realm.class);
        mockStatic(RealmResults.class);


        Realm mockRealm = PowerMockito.mock(Realm.class);

        when(Realm.getDefaultInstance()).thenReturn(mockRealm);
        mrealm = mockRealm;

        when(mrealm.createObject(ImageDTO.class)).thenReturn(new ImageDTO());

        // Create a mock RealmResults
        image = mockRealmResults();


        when(mockRealm.where(ImageDTO.class).findAll()).thenReturn(image);
    }

    @Test
    public void shouldBeAbleToGetDefaultInstance() {
        imageDao.getAllImage();
    }


    @SuppressWarnings("unchecked")
    private static<T extends RealmObject> RealmResults<T> mockRealmResults() {
        return mock(RealmResults.class);
    }

    /*@Test
    public void test1(){
        byte [] b = "123123124242342".getBytes();
        ImageDTO imageDTO = new ImageDTO();
        imageDTO.setSrcPath("/test/path");
        imageDTO.setId(12);
        imageDTO.setCreatedTime("123235346");
        imageDTO.setThumbnail(b);
        List<ImageDTO> list = new LinkedList<>();
        list.add(imageDTO);


        imageDAO.getAllImage();
    }*/
}
